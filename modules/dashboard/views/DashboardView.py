import sys
import getpass

from PySide2.QtWidgets import QWidget, QGridLayout, QMainWindow, QAction, QStatusBar, QMenuBar, QMenu
from PySide2.QtCore import QCoreApplication

# from modules.shared.services.StatusbarService import StatusbarService
from modules.shared import QtWidgetFactory

from modules.tictactoe.TicTacToePresenter import TicTacToePresenter
from modules.tictactoe.views.TicTacToeView import TicTacToeView

class DashboardView(QMainWindow):

    def __init__(self):
        QMainWindow.__init__(self)

        self.resize(1200, 800)

        self.create_menubar()
        self.create_central_widget()

    def create_menubar(self):
        self.menubar = QMenuBar(self)

        self.menuFile = QMenu(self.menubar)
        self.menuFile.setTitle(QCoreApplication.translate("MainWindow", u"File", None))

        actionHome = QAction(self)
        actionHome.setText(QCoreApplication.translate("MainWindow", u"Home", None))
        actionHome.setShortcut(QCoreApplication.translate("MainWindow", u"Ctrl+H", None))
        actionHome.triggered.connect(self.create_central_widget)

        self.menuFile.addAction(actionHome)

        actionExit = QAction(self)
        actionExit.setText(QCoreApplication.translate("MainWindow", u"Exit", None))
        actionExit.setShortcut(QCoreApplication.translate("MainWindow", u"Ctrl+Q", None))
        actionExit.triggered.connect(self.exit_app)

        self.menuFile.addAction(actionExit)

        self.menubar.addAction(self.menuFile.menuAction())

        currentUserMessage = 'User: ' + getpass.getuser() + '   '
        currentUserLabel = QtWidgetFactory.create_label(self.menubar, currentUserMessage)
        self.menubar.setCornerWidget(currentUserLabel)

        self.setMenuBar(self.menubar)

    def create_central_widget(self):
        self.dashboardWidget = QWidget()

        self.ticTacToeButton = QtWidgetFactory.create_dashboard_tile('Tic Tac Toe')
        self.ticTacToeButton.clicked.connect(self.load_tictactoe)


        self.dashboardLayout = QGridLayout()
        self.dashboardLayout.columnCount()
        self.dashboardLayout.addWidget(self.ticTacToeButton, 0, 0)

        self.dashboardWidget.setLayout(self.dashboardLayout)

        self.setCentralWidget(self.dashboardWidget)

    def exit_app(self):
        sys.exit()

    def load_tictactoe(self):
        self.ticTacToePresenter = TicTacToePresenter()
        self.ticTacToeView = TicTacToeView(self.ticTacToePresenter)
        self.ticTacToePresenter.set_view(self.ticTacToeView)

        self.setCentralWidget(self.ticTacToeView)

