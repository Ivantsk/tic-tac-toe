from PySide2.QtGui import QFont
from PySide2.QtWidgets import QToolButton, QLabel, QLineEdit, QPushButton, QSizePolicy, QWidget, QComboBox


def create_button(parentWidget: QWidget, buttonType: str, isEnabled: bool) -> QToolButton:
    button = QToolButton(parentWidget)

    button.setText(buttonType.capitalize())
    button.setMinimumWidth(60)
    button.setMinimumHeight(30)
    button.setEnabled(isEnabled)

    return button


def create_dashboard_tile(text: str) -> QPushButton:
    tile = QPushButton(text)

    # tile.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)

    font = QFont()
    font.setPointSize(12)

    tile.setFont(font)

    return tile


def create_label(parentWidget: QWidget, name='') -> QLabel:
    label = QLabel(parentWidget)
    label.setText(name)

    return label

def create_object_header_label(parentWidget: QWidget, name: str = '') -> QLabel:
    header = QLabel(parentWidget)
    header.setText(name)

    font = QFont()
    font.setPointSize(14)

    header.setFont(font)

    return header


def create_error_label(parentWidget: QWidget, text: str = '') -> QLabel:
    label = QLabel(parentWidget)
    label.setText(text)

    label.setStyleSheet("QLabel { color : red; }")

    font = QFont()
    font.setPointSize(7)

    label.setFont(font)

    return label


def create_line_edit(parentWidget: QWidget) -> QLineEdit:
    lineEdit = QLineEdit(parentWidget)
    lineEdit.setReadOnly(False)

    return lineEdit

def create_combo_box(defaultValue: str) -> QComboBox:
    comboBox = QComboBox()

    comboBox.addItem('Select ' + defaultValue)
    comboBox.model().item(0, 0).setEnabled(False)

    return comboBox
