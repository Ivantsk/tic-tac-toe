import sys

from PySide2.QtWidgets import QApplication, QMessageBox

from modules.dashboard.views.DashboardView import DashboardView

def main():
    app = QApplication(sys.argv)
    app.setApplicationName('Tic tac toe')

    dashboardView = DashboardView()
    dashboardView.show()

    sys.exit(app.exec_())
