from typing import List

from modules.tictactoe.views.ITicTacToeView import ITicTacToeView

class TicTacToePresenter():
    def __init__(self):
        self.view = None
        #TODO Current sign for the player
        
    def set_view(self, view: ITicTacToeView):
        self.view = view

    def new_game(self):
        NewGame = self.view.newGameButton.text()
        print(NewGame)

    def reset(self):
        Reset = self.view.ResetButton.text()
        print(Reset)

    
    def press_button(self, buttonNumber):
        pushButton1 = self.view.boardButton1.text()
        pushButton2 = self.view.boardButton2.text()
        pushButton3 = self.view.boardButton3.text()
        pushButton4 = self.view.boardButton4.text()
        pushButton5 = self.view.boardButton5.text()
        pushButton6 = self.view.boardButton6.text()
        pushButton7 = self.view.boardButton7.text()
        pushButton8 = self.view.boardButton8.text()
        pushButton9 = self.view.boardButton9.text()
        print(buttonNumber)
    
    def counter_button_pressed(self):
        pass
        
    #TODO Make def for the press from the massive.
    #TODO make a counter for clicked cells and a massive for that.