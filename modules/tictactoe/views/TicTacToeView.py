from typing import List
from implements import implements

from PySide2.QtCore import QObject, SIGNAL
from PySide2.QtWidgets import QVBoxLayout
from PySide2.QtWidgets import QWidget, QHBoxLayout, QVBoxLayout, QGroupBox, QGridLayout, QMessageBox, QApplication, QFileDialog, QTreeWidget, QTreeWidgetItem
from PySide2.QtCore import Qt, QRegExp

from modules.tictactoe.TicTacToePresenter import TicTacToePresenter
from modules.shared import QtWidgetFactory
from modules.tictactoe.views.ITicTacToeView import ITicTacToeView

@implements(ITicTacToeView)
class TicTacToeView(ITicTacToeView, QWidget):
    def __init__(self, ticTacToePresenter: TicTacToePresenter) -> None:
        QWidget.__init__(self)

        self.presenter = ticTacToePresenter

        self.init_ui()

        self.initialize_callbacks()

    def init_ui(self) -> None:
        self.layout = QVBoxLayout(self)

        # Creating and saving the different components
        self.header = self.__construct_header()
        self.board = self.__construct_board()
        self.buttonsBar = self.__construct_buttons()
 
        # Adding the components to our main layout
        self.layout.addWidget(self.header)
        self.layout.addWidget(self.board)
        self.layout.addWidget(self.buttonsBar)
 
        # Setting the proportions of the components
        self.__set_layout_proportions([2, 4, 1])

    def initialize_callbacks(self) -> None:
        QObject.connect(self.newGameButton, SIGNAL("clicked()"), self.presenter.new_game)
        QObject.connect(self.ResetButton, SIGNAL("clicked()"), self.presenter.reset)
        QObject.connect(self.boardButton1, SIGNAL("clicked()"),lambda: self.presenter.press_button(1))
        QObject.connect(self.boardButton2, SIGNAL("clicked()"),lambda: self.presenter.press_button(1.2))
        QObject.connect(self.boardButton3, SIGNAL("clicked()"),lambda: self.presenter.press_button(1.3))
        QObject.connect(self.boardButton4, SIGNAL("clicked()"),lambda: self.presenter.press_button(2))
        QObject.connect(self.boardButton5, SIGNAL("clicked()"),lambda: self.presenter.press_button(2.2))
        QObject.connect(self.boardButton6, SIGNAL("clicked()"),lambda: self.presenter.press_button(2.3))
        QObject.connect(self.boardButton7, SIGNAL("clicked()"),lambda: self.presenter.press_button(3))
        QObject.connect(self.boardButton8, SIGNAL("clicked()"),lambda: self.presenter.press_button(3.2))
        QObject.connect(self.boardButton9, SIGNAL("clicked()"),lambda: self.presenter.press_button(3.3))

    def __construct_header(self) -> None:
        headerGroupBox = QGroupBox(self)
        headerGroupBox.setTitle('Header')
 
        headerLayout = QHBoxLayout(self)
 
        # Creating an object. In this case a label.
        self.playerOneLabel = QtWidgetFactory.create_label(self, 'Player 1')
        # Setting the alingnment of the object according to it's layout
        self.playerOneLabel.setAlignment(Qt.AlignCenter)
 
        self.scoreLabel = QtWidgetFactory.create_label(self, '0:0')
        self.scoreLabel.setAlignment(Qt.AlignCenter)
 
        self.playerTwoLabel = QtWidgetFactory.create_label(self, 'Player 2')
        self.playerTwoLabel.setAlignment(Qt.AlignCenter)
 
        # Adding the buttons to the header layout
        headerLayout.addWidget(self.playerOneLabel)
        headerLayout.addWidget(self.scoreLabel)
        headerLayout.addWidget(self.playerTwoLabel)
 
        # Setting the layout as the contents of the groupBox
        headerGroupBox.setLayout(headerLayout)
        return headerGroupBox

    def __construct_board(self):
        boardGroupBox = QGroupBox(self)
        boardGroupBox.setTitle('Board')
 
        boardLayout = QGridLayout(self)
        #Adds the 3x3 boxes into the layout
        boardLayout.setColumnStretch(2, 2)
        #TODO Make a massive here

        #Creating an object. In this case a button.
        self.boardButton1 = QtWidgetFactory.create_button(self, ' ', True)
        #Creating the Size of the Objects
        self.boardButton1.setMinimumSize(150, 150)
        boardButton1 = 1
        self.boardButton2 = QtWidgetFactory.create_button(self, ' ', True)
        self.boardButton2.setMinimumSize(150, 150)
        boardButton2 = 2 
        self.boardButton3 = QtWidgetFactory.create_button(self, ' ', True)
        self.boardButton3.setMinimumSize(150, 150)
        boardButton3 = 3
        self.boardButton4 = QtWidgetFactory.create_button(self, ' ', True)
        self.boardButton4.setMinimumSize(150, 150)
        
        self.boardButton5 = QtWidgetFactory.create_button(self, ' ', True)
        self.boardButton5.setMinimumSize(150, 150)
        self.boardButton6 = QtWidgetFactory.create_button(self, ' ', True)
        self.boardButton6.setMinimumSize(150, 150)
        self.boardButton7 = QtWidgetFactory.create_button(self, ' ', True)
        self.boardButton7.setMinimumSize(150, 150)
        self.boardButton8 = QtWidgetFactory.create_button(self, ' ', True)
        self.boardButton8.setMinimumSize(150, 150)
        self.boardButton9 = QtWidgetFactory.create_button(self, ' ', True)
        self.boardButton9.setMinimumSize(150, 150)
        
        boardArr = [1, 2, 3]
        #Centers the Objects
        boardLayout.setAlignment(Qt.AlignCenter)
        
        #Adds the elements to the layout
        boardLayout.addWidget(self.boardButton1)
        boardLayout.addWidget(self.boardButton2)
        boardLayout.addWidget(self.boardButton3)
        boardLayout.addWidget(self.boardButton4)
        boardLayout.addWidget(self.boardButton5)
        boardLayout.addWidget(self.boardButton6)
        boardLayout.addWidget(self.boardButton7)
        boardLayout.addWidget(self.boardButton8)
        boardLayout.addWidget(self.boardButton9)

        boardGroupBox.setLayout(boardLayout)
        return boardGroupBox
 
    def __construct_buttons(self):
        buttonsBarGroupBox = QGroupBox(self)
        buttonsBarGroupBox.setTitle('Buttons bar')
 
        buttonsBarLayout = QHBoxLayout(self)
 
        #Creates the buttons here
        self.newGameButton = QtWidgetFactory.create_button(self, 'New game', True)
        self.ResetButton = QtWidgetFactory.create_button(self, 'Reset', True)
 
 
        #Adds the elements to the layout
        buttonsBarLayout.addWidget(self.ResetButton)
        buttonsBarLayout.addWidget(self.newGameButton)
 
        buttonsBarGroupBox.setLayout(buttonsBarLayout)
        return buttonsBarGroupBox

    def __set_layout_proportions(self, proportions: List[int]) -> None:
        for i, proportion in enumerate(proportions):
            self.layout.setStretch(i, proportion)
